import sys
from datetime import datetime, timedelta
import argparse
import logging
# Only used for type hinting.
import typing as T

logging.basicConfig(level=logging.DEBUG)


parser = argparse.ArgumentParser()
parser.add_argument("name", nargs="?", default="World")
parser.add_argument("-o", "--offset", type=int, default=0)

# Create a new logger
log = logging.getLogger(__name__)

def say_hello(name : T.Any, hour_offset : int):
    # Boundary checking.
    # NOTE: typically we'd want to raise an exception here,
    # such as a RuntimeException. However, to demonstrate logging,
    # we'll just log an error, then exit gracefully.
    # NOTE: Actually, more than likely the proper way to do this
    # would be to do bounds checking via argparser.
    if abs(hour_offset) > 23:
        log.error("`hour_offset` must be less than 24!")
        sys.exit(1)
    curr_time = datetime.now()
    check_time = curr_time + timedelta(hours=hour_offset)
    log.debug("Current time: %s", curr_time.strftime("%c"))
    log.debug("Offset in hours: %s", hour_offset)
    log.debug("Time to check: %s", check_time)
    if check_time.hour > 16:
        print(f"Closing time, {name}. I'm done for the day.")
        return
    if check_time.hour > 12:
        print(f"Good afternoon, {name}. How was lunch?")
        return
    print(f"{name}, I need coffee.")
    return

def main(args):
    hour_offset = int(args.offset)
    name = args.name
    say_hello(name, hour_offset)

if __name__=='__main__':
    args = parser.parse_args()
    main(args)