import argparse

parser = argparse.ArgumentParser()

parser.add_argument("name", nargs="?", default="World")


def main(args):
    print(f"Hello, {args.name}!")


if __name__ == "__main__":
    args = parser.parse_args()
    main(args)